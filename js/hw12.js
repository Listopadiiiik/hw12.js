// Теоретичні питання

// Тому що в input користувач може ввести що завгодно (наприклад це може бути вставлений текст),
//  і виявляти те що ввів користувач тільки через події клавіши не достатньо.

// Завдання

let button = document.querySelectorAll('.btn');
let activeButton = document.querySelectorAll('.active');

document.addEventListener('keypress', function(event){
    button.forEach(function(item){
        let letter = item.innerText;
        let smallLetter = letter.toLowerCase();
        if(letter == event.key || smallLetter == event.key){
           item.classList.toggle('active');   
    } else if(item.classList.contains('active')){
        item.classList.add('visited');
    }
    })
})

